package com.telnet.domain;

import java.util.List;

public class Message {

    private String id;
    private String textMessage;
    private String photoMessage;
    private String channel;
    private String timeCreation;
    private String formattedTimeCreation;
    private String avatar;
    private int votes;
    private long numberOfComments;
    private List<String> links;
    private List<String> hashTags;
    private List<String> mediaFiles;
    private boolean indexed;
    private TopicClass topicClass;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getPhotoMessage() {
        return photoMessage;
    }

    public void setPhotoMessage(String photoMessage) {
        this.photoMessage = photoMessage;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimeCreation() {
        return timeCreation;
    }

    public void setTimeCreation(String timeCreation) {
        this.timeCreation = timeCreation;
    }

    public String getFormattedTimeCreation() {
        return formattedTimeCreation;
    }

    public void setFormattedTimeCreation(String formattedTimeCreation) {
        this.formattedTimeCreation = formattedTimeCreation;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public long getNumberOfComments() {
        return numberOfComments;
    }

    public void setNumberOfComments(long numberOfComments) {
        this.numberOfComments = numberOfComments;
    }

    public List<String> getLinks() {
        return links;
    }

    public void setLinks(List<String> links) {
        this.links = links;
    }

    public boolean isIndexed() {
        return indexed;
    }

    public void setIndexed(boolean indexed) {
        this.indexed = indexed;
    }

    public TopicClass getTopicClass() {
        return topicClass;
    }

    public void setTopicClass(TopicClass topicClass) {
        this.topicClass = topicClass;
    }

    public List<String> getMediaFiles() {
        return mediaFiles;
    }

    public void setMediaFiles(List<String> mediaFiles) {
        this.mediaFiles = mediaFiles;
    }

    public List<String> getHashTags() {
        return hashTags;
    }

    public void setHashTags(List<String> hashTags) {
        this.hashTags = hashTags;
    }
}
