package com.telnet.domain;

import java.time.LocalDateTime;

public class Comment {

    private String id;
    private String author;
    private String formattedTimeCreation;
    private LocalDateTime timeCreation;
    private String text;
    private String messageId;
    private int votes;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public String getFormattedTimeCreation() {
        return formattedTimeCreation;
    }

    public void setFormattedTimeCreation(String formattedTimeCreation) {
        this.formattedTimeCreation = formattedTimeCreation;
    }

    public LocalDateTime getTimeCreation() {
        return timeCreation;
    }

    public void setTimeCreation(LocalDateTime timeCreation) {
        this.timeCreation = timeCreation;
    }
}
