package com.telnet.domain;

public enum TopicClass {
    TELEGRAM("Телеграм"), COVID("COVID"), POLITICS("Политика"),
    OTHER("Другое"), SOROS("Соросята"), ELECTIONS("Выборы"),
    BELARUS("Беларусь"), CORRUPTION("Коррупция"), WAR("Война");

    String localized;

    TopicClass(String loc){
        this.localized = loc;
    }
}
