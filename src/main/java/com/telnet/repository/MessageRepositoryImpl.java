package com.telnet.repository;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Sorts;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.SortingParams;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.eq;

@Repository
public class MessageRepositoryImpl implements MessageRepository {

    private final MongoCollection<Document> messagesCollection;
    private final RedisTemplate redisTemplate;

    public MessageRepositoryImpl(MongoCollection<Document> messagesCollection, RedisTemplate redisTemplate) {
        this.messagesCollection = messagesCollection;
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void saveMessage(List<Document> messages) {
        messages.forEach(messageDocument -> {
            ObjectId objectId = new ObjectId();
            messageDocument.put("_id", objectId);
            messagesCollection.insertOne(messageDocument);

            redisTemplate.execute(jedis -> {
                jedis.sadd("indices", objectId.toString());
                jedis.hset(objectId.toString(), "vote", "0");
            });
        });
    }

    @Override
    public List<Document> getMessages(Integer skip) {
        FindIterable<Document> cursor = messagesCollection.find()
                .sort(Sorts.descending("timeCreation"))
                .skip(skip)
                .limit(10);
        return getListOfDocuments(cursor);
    }

    @Override
    public List<Document> getFreshMessages(Integer skip) {
        FindIterable<Document> cursor = messagesCollection.find()
                .sort(Sorts.descending("timeCreation"))
                .skip(skip)
                .limit(10);
        return getListOfDocuments(cursor);
    }

    @Override
    public Document getLatestMessage() {
        return messagesCollection.find().sort(Sorts.descending("timeCreation")).first();
    }

    @Override
    public Optional<Document> getMessage(String id) {
        try {
            Document document = messagesCollection.find(eq("_id", new ObjectId(id))).first();
            return Optional.ofNullable(document);
        } catch (IllegalArgumentException ex) {
            return Optional.empty();
        }
    }

    @Override
    public List<String> getPopularMessages(int offset) {
        return redisTemplate.execute(jedis -> {
            return jedis.sort("indices", new SortingParams().alpha().limit(offset, 10));
        });
    }

    @Override
    public List<Document> getMessagesByTopic(String topic, Integer skip) {
        FindIterable<Document> cursor = messagesCollection.find(eq("topicClass", topic))
                .sort(Sorts.descending("timeCreation"))
                .skip(skip)
                .limit(10);
        return getListOfDocuments(cursor);
    }

    @Override
    public void decrementRating(String messageId) {
        redisTemplate.execute(jedis -> {
            if (isKeyNotExisted(messageId)) {
                jedis.sadd("indices", messageId);
            }
            jedis.hincrBy(messageId, "vote", -1);
        });
    }

    @Override
    public int getNumberOfVotes(String messageId) {
        String voteVal = redisTemplate.execute(jedis -> {
            return jedis.hget(messageId, "vote");
        });
        return StringUtils.isEmpty(voteVal) ? 0 : Integer.parseInt(voteVal);
    }

    @Override
    public void incrementRating(String messageId) {
        redisTemplate.execute(jedis -> {
            if (isKeyNotExisted(messageId)) {
                jedis.sadd("indices", messageId);
            }
            jedis.hincrBy(messageId, "vote", 1);
        });
    }

    private List<Document> getListOfDocuments(FindIterable<Document> cursor) {
        return StreamSupport.stream(cursor.spliterator(), false)
                .collect(Collectors.toList());
    }

    private boolean isKeyNotExisted(String messageId) {
        String rating = redisTemplate.execute(jedis -> {
            return jedis.hget(messageId, "rating");
        });
        return StringUtils.isEmpty(rating);
    }

}
