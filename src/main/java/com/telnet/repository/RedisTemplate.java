package com.telnet.repository;

import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.function.Consumer;
import java.util.function.Function;

@Component
public final class RedisTemplate {

    private final JedisPool jedisPool;

    public RedisTemplate(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    private Jedis getJedis() {
        return jedisPool.getResource();
    }

    private void close(Jedis jedis) {
        jedis.close();
    }

    protected void execute(Consumer<Jedis> execute) {
        Jedis jedis = getJedis();
        execute.accept(jedis);
        close(jedis);
    }

    protected <T> T execute(Function<Jedis, T> execute) {
        Jedis jedis = getJedis();
        T result = execute.apply(jedis);
        close(jedis);
        return result;
    }

}
