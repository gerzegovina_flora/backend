package com.telnet.repository;

import org.bson.Document;

import java.util.List;
import java.util.Optional;

public interface CommentaryRepository {

    String saveComment(Document comment);

    Optional<Document> getComment(String commentId);

    List<Document> getFreshComments(String messageId, Integer skip);

    List<Document> getPopularComments(String messageId, Integer skip);

    long getNumberOfComments(String messageId);

    void decrementRating(String commentId);

    int getNumberOfVotes(String commentId);

    void incrementRating(String commentId);

}
