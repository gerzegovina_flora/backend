package com.telnet.repository;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Sorts;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.eq;

@Repository
public class CommentaryRepositoryImpl implements CommentaryRepository {

    private final MongoCollection<Document> commentsCollection;
    private final RedisTemplate redisTemplate;

    public CommentaryRepositoryImpl(MongoCollection<Document> commentsCollection, RedisTemplate redisTemplate) {
        this.commentsCollection = commentsCollection;
        this.redisTemplate = redisTemplate;
    }

    @Override
    public String saveComment(Document comment) {
        commentsCollection.insertOne(comment);
        return comment.get("_id").toString();
    }

    @Override
    public Optional<Document> getComment(String commentId) {
        try {
            Document document = commentsCollection.find(eq("_id", new ObjectId(commentId))).first();
            return Optional.ofNullable(document);
        } catch (IllegalArgumentException ex) {
            return Optional.empty();
        }
    }

    @Override
    public List<Document> getFreshComments(String messageId, Integer skip) {
        FindIterable<Document> cursor = commentsCollection.find(eq("messageId", messageId))
                .sort(Sorts.descending("timeCreation"))
                .skip(skip)
                .limit(10);
        return getListOfDocuments(cursor);
    }

    @Override
    public List<Document> getPopularComments(String messageId, Integer skip) {
        FindIterable<Document> cursor = commentsCollection.find(eq("messageId", messageId));
        return getListOfDocuments(cursor);
    }

    @Override
    public long getNumberOfComments(String messageId) {
        return commentsCollection.countDocuments(eq("messageId", messageId));
    }

    @Override
    public void decrementRating(String commentId) {
        redisTemplate.execute(jedis -> {
            jedis.hincrBy("vote", commentId, -1);
        });
    }

    @Override
    public int getNumberOfVotes(String commentId) {
        String vote = redisTemplate.execute(jedis -> {
            return jedis.hget("vote", commentId);
        });
        return StringUtils.isEmpty(vote) ? 0 : Integer.parseInt(vote);
    }

    @Override
    public void incrementRating(String commentId) {
        redisTemplate.execute(jedis -> {
            jedis.hincrBy("vote", commentId, 1);
        });
    }

    private List<Document> getListOfDocuments(FindIterable<Document> cursor) {
        return StreamSupport.stream(cursor.spliterator(), false)
                .collect(Collectors.toList());
    }
}
