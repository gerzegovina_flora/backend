package com.telnet.repository;

import org.bson.Document;

import java.util.List;
import java.util.Optional;

public interface MessageRepository {

    void saveMessage(List<Document> messages);

    List<Document> getMessages(Integer skip);

    List<Document> getFreshMessages(Integer oldMessagesCount);

    Document getLatestMessage();

    void incrementRating(String id);

    void decrementRating(String id);

    int getNumberOfVotes(String id);

    Optional<Document> getMessage(String id);

    List<String> getPopularMessages(int offset);

    List<Document> getMessagesByTopic(String topic, Integer skip);
}
