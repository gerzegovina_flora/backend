package com.telnet.service;

import com.telnet.domain.Message;
import com.telnet.repository.MessageRepository;
import org.bson.Document;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class MessageServiceImpl implements MessageService {

    private final ConversionService conversionService;
    private final MessageRepository messageRepository;


    public MessageServiceImpl(MessageRepository messageRepository, ConversionService conversionService) {
        this.messageRepository = messageRepository;
        this.conversionService = conversionService;
    }

    @Override
    public void saveMessages(List<Message> messageList) {
        List<Message> messages = getFilteredMessage(messageList);
        List<Document> documents = messages.stream()
                .map(this::convertMessageToDocument)
                .collect(Collectors.toList());
        messageRepository.saveMessage(documents);
    }

    @Override
    public List<Message> getMessages(Integer skip) {
        List<Document> messageDocuments = messageRepository.getMessages(skip);
        return messageDocuments.stream()
                .map(this::convertDocumentToMessage)
                .collect(Collectors.toList());
    }

    @Override
    public List<Message> getPopularMessages(Integer skip) {
        return messageRepository.getPopularMessages(skip).stream()
                .map(this::getMessage)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public List<Message> getFreshMessages(Integer skip) {
        List<Document> messageDocuments = messageRepository.getFreshMessages(skip);
        return messageDocuments.stream()
                .map(this::convertDocumentToMessage)
                .collect(Collectors.toList());
    }

    @Override
    public boolean checkFreshMessages(LocalDateTime localDateTime) {
        Message message = convertDocumentToMessage(messageRepository.getLatestMessage());
        return LocalDateTime.parse(message.getTimeCreation()).equals(localDateTime);
    }

    @Override
    public void likeMessage(String id) {
        messageRepository.incrementRating(id);
    }

    @Override
    public void dislikeMessage(String id) {
        messageRepository.decrementRating(id);
    }

    @Override
    public int getNumberOfVotes(String id) {
        return messageRepository.getNumberOfVotes(id);
    }

    @Override
    public Message getMessage(String id) {
        return messageRepository.getMessage(id)
                .map(this::convertDocumentToMessage)
                .orElse(null);
    }

    @Override
    public List<Message> getMessagesByTopic(String topic, Integer skip) {
        List<Document> messageDocuments = messageRepository.getMessagesByTopic(topic, skip);
        return messageDocuments.stream()
                .map(this::convertDocumentToMessage)
                .collect(Collectors.toList());
    }

    private List<Message> getFilteredMessage(List<Message> messages) {
        return messages.stream()
                .filter(message -> !StringUtils.isEmpty(message.getChannel()))
                .filter(message -> !(StringUtils.isEmpty(message.getPhotoMessage()) && StringUtils.isEmpty(message.getTextMessage())))
                .collect(Collectors.toList());
    }

    private Message convertDocumentToMessage(Document document) {
        return conversionService.convert(document, Message.class);
    }

    private Document convertMessageToDocument(Message message) {
        return conversionService.convert(message, Document.class);
    }
}
