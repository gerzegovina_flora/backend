package com.telnet.service;

import com.telnet.domain.Message;

import java.time.LocalDateTime;
import java.util.List;

public interface MessageService {

    void saveMessages(List<Message> messages);

    List<Message> getMessages(Integer skip);

    List<Message> getPopularMessages(Integer skip);

    List<Message> getFreshMessages(Integer skip);

    boolean checkFreshMessages(LocalDateTime localDateTime);

    void likeMessage(String id);

    void dislikeMessage(String id);

    int getNumberOfVotes(String id);

    Message getMessage(String id);

    List<Message> getMessagesByTopic(String topic, Integer skip);
}
