package com.telnet.service;

import com.telnet.domain.Comment;
import com.telnet.repository.CommentaryRepository;
import org.bson.Document;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentaryServiceImpl implements CommentaryService {

    private final CommentaryRepository commentaryRepository;
    private final ConversionService conversionService;

    public CommentaryServiceImpl(CommentaryRepository commentaryRepository, ConversionService conversionService) {
        this.commentaryRepository = commentaryRepository;
        this.conversionService = conversionService;
    }

    @Override
    public Comment createComment(Comment comment) {
        Document document = convertDocumentToComment(comment);
        String commentId = commentaryRepository.saveComment(document);
        return getComment(commentId);
    }

    @Override
    public List<Comment> getFreshComments(String messageId, Integer skip) {
        return commentaryRepository.getFreshComments(messageId, skip).stream()
                .map(this::convertDocumentToComment)
                .peek(comment -> comment.setVotes(commentaryRepository.getNumberOfVotes(comment.getId())))
                .collect(Collectors.toList());
    }

    @Override
    public List<Comment> getPopularComments(String messageId, Integer skip) {
        return commentaryRepository.getPopularComments(messageId, skip).stream()
                .map(this::convertDocumentToComment)
                .peek(comment -> comment.setVotes(commentaryRepository.getNumberOfVotes(comment.getId())))
                .sorted((comm1, comm2) -> Long.compare(comm2.getVotes(), comm1.getVotes()))
                .collect(Collectors.toList());
    }

    @Override
    public void likeComment(String commentId) {
        commentaryRepository.incrementRating(commentId);
    }

    @Override
    public void dislikeComment(String commentId) {
        commentaryRepository.decrementRating(commentId);
    }

    @Override
    public Comment getComment(String commentId) {
        return commentaryRepository.getComment(commentId)
                .map(this::convertDocumentToComment)
                .orElse(null);
    }

    private Comment convertDocumentToComment(Document document) {
        return conversionService.convert(document, Comment.class);
    }

    private Document convertDocumentToComment(Comment comment) {
        return conversionService.convert(comment, Document.class);
    }

}
