package com.telnet.service;

import com.telnet.domain.Message;

import java.util.List;

public interface SearchService {

    List<Message> getSearchedMessages(String text);

}
