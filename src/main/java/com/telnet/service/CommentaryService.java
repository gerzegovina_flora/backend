package com.telnet.service;

import com.telnet.domain.Comment;

import java.util.List;

public interface CommentaryService {

    Comment createComment(Comment comment);

    List<Comment> getFreshComments(String messageId, Integer skip);

    List<Comment> getPopularComments(String messageId, Integer skip);

    void likeComment(String commentId);

    void dislikeComment(String commentId);

    Comment getComment(String commentId);

}
