package com.telnet.service;

import com.telnet.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    private MessageService messageService;

    @Override
    public List<Message> getSearchedMessages(String text) {
        return Collections.emptyList();
    }
}
