package com.telnet.exception;

public class RepositoryConfigurationDependency extends RuntimeException {

    public RepositoryConfigurationDependency(String message) {
        super(message);
    }
}
