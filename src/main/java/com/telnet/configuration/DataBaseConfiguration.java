package com.telnet.configuration;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.telnet.exception.RepositoryConfigurationDependency;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.PostConstruct;

@Configuration
public class DataBaseConfiguration {

    private static final String MONGO_DB_NAME = "acme";
    private static final String MONGO_URL = "MONGO_URL";
    private static final String REDIS_URL = "REDIS_URL";
    private static final String LOCALHOST = "localhost";

    private final Environment env;
    private MongoClient mongoClient;

    public DataBaseConfiguration(Environment env) {
        this.env = env;
    }

    @Bean("jedis")
    public JedisPool getJedisPool() {
        String redisPath = env.getProperty(REDIS_URL);
        return new JedisPool(new JedisPoolConfig(), StringUtils.isNotEmpty(redisPath) ? redisPath : LOCALHOST);
    }

    @Bean("messagesCollection")
    public MongoCollection<Document> getMessagesCollection() {
        if (mongoClient == null) {
            throw new RepositoryConfigurationDependency("Mongo client was not initialized");
        }
        return mongoClient.getDatabase(MONGO_DB_NAME).getCollection("messages");
    }

    @Bean("commentsCollection")
    public MongoCollection<Document> getCommentsCollection() {
        if (mongoClient == null) {
            throw new RepositoryConfigurationDependency("Mongo client was not initialized");
        }
        return mongoClient.getDatabase(MONGO_DB_NAME).getCollection("comments");
    }

    @PostConstruct
    public void init() {
        String mongoPath = env.getProperty(MONGO_URL);
        mongoClient = new MongoClient(StringUtils.isNotEmpty(mongoPath) ? mongoPath : LOCALHOST);
    }

}
