package com.telnet.converter;

import com.telnet.domain.Comment;
import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.Locale;

@Component
public class DocumentToCommentConverter implements Converter<Document, Comment> {

    @Override
    public Comment convert(Document document) {
        Comment comment = new Comment();
        comment.setAuthor(document.getString("author"));
        comment.setId(document.get("_id").toString());
        comment.setText(document.getString("text"));
        comment.setMessageId(document.getString("messageId"));
        comment.setAuthor(document.getString("author"));

        LocalDateTime timeCreation = convertDate(document.getDate("timeCreation"));
        String formattedDate = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
                .withLocale(new Locale("ru"))
                .format(timeCreation);
        comment.setTimeCreation(timeCreation);
        comment.setFormattedTimeCreation(formattedDate);
        return comment;
    }

    private LocalDateTime convertDate(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
}
