package com.telnet.converter;

import com.telnet.domain.Message;
import com.telnet.domain.TopicClass;
import com.telnet.repository.CommentaryRepository;
import com.telnet.repository.MessageRepository;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
public class DocumentToMessageConverter implements Converter<Document, Message> {

    private final CommentaryRepository commentaryRepository;
    private final MessageRepository messageRepository;

    public DocumentToMessageConverter(CommentaryRepository commentaryRepository, MessageRepository messageRepository) {
        this.commentaryRepository = commentaryRepository;
        this.messageRepository = messageRepository;
    }

    @Override
    public Message convert(Document document) {
        Message message = new Message();
        message.setChannel(document.getString("channelName"));
        message.setPhotoMessage(document.getString("photoMessage"));
        message.setTextMessage(document.getString("textMessage"));
        message.setId(document.get("_id").toString());
        message.setNumberOfComments(commentaryRepository.getNumberOfComments(message.getId()));
        message.setVotes(messageRepository.getNumberOfVotes(message.getId()));

        LocalDateTime timeCreation = convertDate(document.getDate("timeCreation"));
        message.setTimeCreation(timeCreation.toString());

        message.setFormattedTimeCreation(getFormattedDate(timeCreation));
        message.setAvatar("data:image/jpeg;base64,".concat(document.getString("avatar")));
        message.setLinks(document.getList("links", String.class, Collections.emptyList()));
        message.setHashTags(document.getList("hashTags", String.class, Collections.emptyList()));

        populateMediaFiles(document, message);
        populateTopicClass(document, message);
        return message;
    }

    private void populateTopicClass(Document document, Message message) {
        String topicClass = document.getString("topicClass");
        message.setTopicClass(StringUtils.isEmpty(topicClass) ? null : TopicClass.valueOf(topicClass));
    }

    void populateMediaFiles(Document document, Message message) {
        List<String> mediaFiles = document.getList("mediaFiles", String.class, Collections.emptyList()).stream()
                .map("data:image/jpeg;base64,"::concat)
                .collect(Collectors.toList());
        message.setMediaFiles(mediaFiles);
    }

    private String getFormattedDate(LocalDateTime timeCreation) {
        return DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                .withLocale(new Locale("ru"))
                .format(timeCreation);
    }

    private LocalDateTime convertDate(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
}
