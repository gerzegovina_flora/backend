package com.telnet.converter;

import com.telnet.domain.Comment;
import com.telnet.util.RandomWordGenerator;
import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CommentToDocumentConverter implements Converter<Comment, Document> {
    @Override
    public Document convert(Comment comment) {
        Document document = new Document();
        document.put("text", comment.getText());
        document.put("messageId", comment.getMessageId());
        document.put("timeCreation", LocalDateTime.now());
        document.put("author", RandomWordGenerator.getRandomNickName());
        return document;
    }
}
