package com.telnet.controller;

import com.telnet.domain.Comment;
import com.telnet.service.CommentaryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class CommentaryController {

    private final CommentaryService commentaryService;

    public CommentaryController(CommentaryService commentaryService) {
        this.commentaryService = commentaryService;
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/comments/popular", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<Comment> getPopularComments(@RequestParam(defaultValue = "0") Integer skip, @RequestParam String messageId) {
        return commentaryService.getPopularComments(messageId, skip);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/comments/date", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<Comment> getFreshComments(@RequestParam(defaultValue = "0") Integer skip, @RequestParam String messageId) {
        return commentaryService.getFreshComments(messageId, skip);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/comment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody
    Comment saveMessages(@RequestBody Comment comment) {
        return commentaryService.createComment(comment);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/comment/like", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public void likeComment(@RequestBody Map<String, String> idMessage) {
        commentaryService.likeComment(idMessage.get("id"));
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/comment/dislike", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public void dislikeComment(@RequestBody Map<String, String> idMessage) {
        commentaryService.dislikeComment(idMessage.get("id"));
    }

}
