package com.telnet.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telnet.domain.Message;
import com.telnet.domain.TopicClass;
import com.telnet.service.MessageService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@RestController("/")
public class MessageController {

    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<Message> getMessages(@RequestParam(defaultValue = "0") Integer skip, @RequestParam(defaultValue = "false") boolean update) {
        // TODO: remove condition. use only one method
        if (update) {
            return messageService.getFreshMessages(skip);
        }
        return messageService.getMessages(skip);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/messages/topics", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public String getTopics() throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(TopicClass.values());
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/messages/topic/{topic}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<Message> getMessagesByTopic(@RequestParam(defaultValue = "0") Integer skip, @PathVariable String topic) {
        return messageService.getMessagesByTopic(topic, skip);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/messages/popular", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<Message> getPopular(@RequestParam(defaultValue = "0") Integer skip) {
        return messageService.getPopularMessages(skip);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/message/{messageId}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public Message getMessageInfo(@PathVariable String messageId) {
        return messageService.getMessage(messageId);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/isUpToDate", method = RequestMethod.GET)
    public @ResponseBody
    boolean freshMessagesCheck(@RequestParam String date) {
        return messageService.checkFreshMessages(LocalDateTime.parse(date));
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/like", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public void likeMessage(@RequestBody Map<String, String> idMessage) {
        messageService.likeMessage(idMessage.get("id"));
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/dislike", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public void dislikeMessage(@RequestBody Map<String, String> idMessage) {
        messageService.dislikeMessage(idMessage.get("id"));
    }

}
