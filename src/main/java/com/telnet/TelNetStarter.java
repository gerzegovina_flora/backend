package com.telnet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

@SpringBootApplication
public class TelNetStarter {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(TelNetStarter.class);
        app.setDefaultProperties(Collections
                .singletonMap("server.port", "8089"));
        app.run(args);
    }

}
