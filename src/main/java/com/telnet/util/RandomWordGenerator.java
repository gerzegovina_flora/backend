package com.telnet.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class RandomWordGenerator {

    private RandomWordGenerator() {
    }

    public static List<String> nickName = new ArrayList<String>() {
        {
            add("ЦіРукиНічогоНеКрали");
            add("Бандитам – тюрьмы");
            add("КуляВЛоб");
            add("Вона працює");
            add("Увімкни Україну");
            add("Жити по-новому");
            add("ДоверяйДелам");
            add("ВМенеВжеПиталиПіська");
            add("СмотретьМогутНеТолькоЛишьВсе");
            add("Ёлка");
            add("Кровосиси");
            add("БімбаВВагіні");
        }
    };

    public static String getRandomNickName() {
        return nickName.get(new Random().nextInt(nickName.size()));
    }

}
